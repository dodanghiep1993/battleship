public class Battleship extends  Ship {
    private int xposition;
    private int yposition;
    public int getXposition() {
        return xposition;
    }

    public void setXposition(int xposition) {
        this.xposition = xposition;
    }

    public int getYposition() {
        return yposition;
    }

    public void setYposition(int yposition) {
        this.yposition = yposition;
    }

    @Override
    public String[][] Creatship(String[][] grid, int x, int y, String horv) {
        switch (horv) {
            case "v":
                if (x <= 6) {
                    grid[x][y] = "b";
                    grid[x+1][y] = "b";
                    grid[x+2][y] = "b";
                    grid[x+3][y] = "b";
                    break;
                } else {
                    grid[x][y] = "b";
                    grid[x-1][y] = "b";
                    grid[x-2][y] = "b";
                    grid[x-3][y] = "b";
                    break;
                }
            case "h":
                if (y <= 6) {
                    grid[x][y] = "b";
                    grid[x][y+1] = "b";
                    grid[x][y+2] = "b";
                    grid[x][y+3] = "b";
                    break;
                } else {
                    grid[x][y] = "b";
                    grid[x][y-1] = "b";
                    grid[x][y-2] = "b";
                    grid[x][y-3] = "b";
                    break;
                }
        }
        return grid;
    }
}
