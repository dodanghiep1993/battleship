import java.util.Scanner;

public class GameWorld {
    public static int bas1 = 4;
    public static int carr1 = 5;
    public static int bas2 = 4;
    public static int carr2 = 5;
    public static void main(String[] args) {
        Scanner getInput = new Scanner(System.in);
        Scanner getInput2 = new Scanner(System.in);
        Scanner getInput3 = new Scanner(System.in);
        Scanner getInput4 = new Scanner(System.in);
        Scanner getInput5 = new Scanner(System.in);
        Scanner getInput6 = new Scanner(System.in);
        Board b = new Board();
        Board b1 = new Board();
        Board b2 = new Board();
        Board b3 = new Board();
        Carrier c = new Carrier();
        Battleship bas = new Battleship();

        //1st Prompt
        System.out.println("\n" + "Battleship Multiplayer");
        System.out.print("Enter Player 1 name: ");
        String p1name = "";
        try {
            if (getInput.hasNextInt()) {
                throw new Exception();
            } else {
                p1name = getInput.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        System.out.print("Enter Player 2 name: ");
        String p2name = "";
        try {
            if (getInput.hasNextInt()) {
                throw new Exception();
            } else {
                p2name = getInput.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        System.out.println();
        b.Printboard(b.getGrid());
        System.out.println();

        //2nd Prompt for 1st player to enter the coordinates
        System.out.println(p1name + " Please enter the coordinates for your ships");
        System.out.println();
        System.out.print("Enter the coordinates for the carrier: ");
        String shipPositions = "";
        try {
            if (getInput.hasNextInt()) {
                throw new Exception();
            } else {
                shipPositions = getInput.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        String getcoor = getXandY(shipPositions);
        int x = Character.getNumericValue(getcoor.charAt(1));
        int y = Character.getNumericValue(getcoor.charAt(3));
        System.out.print("Place horizontally or vertically (h or v)? ");
        String horv = "";
        try {
            if (getInput.hasNextInt()) {
                throw new Exception();
            } else {
                horv = getInput.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        System.out.println();
        b.setGrid(c.Creatship(b.getGrid(), x, y, horv));
        b.Printboard(b.getGrid());

        System.out.println();
        System.out.print("Enter the coordinates for the battleship: ");
        String shipPositions2 = "";
        try {
            if (getInput2.hasNextInt()) {
                throw new Exception();
            } else {
                shipPositions2 = getInput2.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        String getcoor2 = getXandY(shipPositions2);
        int xb = Character.getNumericValue(getcoor2.charAt(1));
        int yb = Character.getNumericValue(getcoor2.charAt(3));
        while (true) {
            if (b.getGrid()[xb][yb] == "c") {
                System.out.print("carrier is already located there, enter another location: ");
                String shipPositionsagain = "";
                try {
                    if (getInput2.hasNextInt()) {
                        throw new Exception();
                    } else {
                        shipPositionsagain = getInput2.next();
                    }
                } catch (Exception e) {
                    System.out.println();
                    System.out.println("Invalid input!");
                    System.exit(0);
                }
                String getcooragain = getXandY(shipPositionsagain);
                xb = Character.getNumericValue(getcooragain.charAt(1));
                yb = Character.getNumericValue(getcooragain.charAt(3));
            } else {
                break;
            }
        }
        System.out.print("Place horizontally or vertically (h or v)? ");
        String horv2 = "";
        try {
            if (getInput2.hasNextInt()) {
                throw new Exception();
            } else {
                horv2 = getInput2.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        System.out.println();
        b.setGrid(bas.Creatship(b.getGrid(), xb, yb, horv2));
        b.Printboard(b.getGrid());
        System.out.println();
        b1.Printboard(b1.getGrid());
        System.out.println();

        //2nd Prompt for 2st player to enter the coordinates
        System.out.println(p2name + " Please enter the coordinates for your ships");
        System.out.println();
        System.out.print("Enter the coordinates for the carrier: ");
        String shipPositions3 = "";
        try {
            if (getInput3.hasNextInt()) {
                throw new Exception();
            } else {
                shipPositions3 = getInput3.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        String getcoor3 = getXandY(shipPositions3);
        int x1 = Character.getNumericValue(getcoor3.charAt(1));
        int y1 = Character.getNumericValue(getcoor3.charAt(3));
        System.out.print("Place horizontally or vertically (h or v)? ");
        String horv3 = "";
        try {
            if (getInput3.hasNextInt()) {
                throw new Exception();
            } else {
                horv3 = getInput3.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        System.out.println();
        b1.setGrid(c.Creatship(b1.getGrid(), x1, y1, horv3));
        b1.Printboard(b1.getGrid());

        System.out.println();
        System.out.print("Enter the coordinates for the battleship: ");
        String shipPositions4 = "";
        try {
            if (getInput4.hasNextInt()) {
                throw new Exception();
            } else {
                shipPositions4 = getInput4.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        String getcoor4 = getXandY(shipPositions4);
        int xb1 = Character.getNumericValue(getcoor4.charAt(1));
        int yb1 = Character.getNumericValue(getcoor4.charAt(3));
        while (true) {
            if (b1.getGrid()[xb1][yb1] == "c") {
                System.out.print("carrier is already located there, enter another location: ");
                String shipPositionsagain1 = "";
                try {
                    if (getInput2.hasNextInt()) {
                        throw new Exception();
                    } else {
                        shipPositionsagain1 = getInput2.next();
                    }
                } catch (Exception e) {
                    System.out.println();
                    System.out.println("Invalid input!");
                    System.exit(0);
                }
                String getcooragain1 = getXandY(shipPositionsagain1);
                xb1 = Character.getNumericValue(getcooragain1.charAt(1));
                yb1 = Character.getNumericValue(getcooragain1.charAt(3));
            } else {
                break;
            }
        }
        System.out.print("Place horizontally or vertically (h or v)? ");
        String horv4 = "";
        try {
            if (getInput4.hasNextInt()) {
                throw new Exception();
            } else {
                horv4 = getInput4.next();
            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Invalid input!");
            System.exit(0);
        }
        System.out.println();
        b1.setGrid(bas.Creatship(b1.getGrid(), xb1, yb1, horv4));
        b1.Printboard(b1.getGrid());
        System.out.println();
        System.out.println("###################");
        System.out.println("#THE BATTLE BEGINS#");
        System.out.println("###################");
        System.out.println();

        //3rd prompt Battle
        BATTLE:
        while(true) {
            //First player turn
            b2.Printboard(b2.getGrid());
            System.out.println();
            System.out.print(p1name + " enter the coordinates for an attack: ");
            String attackposition1 = getInput5.nextLine();
            System.out.println();
            String getcoor5 = getXandY(attackposition1);
            int xa1 = Character.getNumericValue(getcoor5.charAt(1));
            int ya1 = Character.getNumericValue(getcoor5.charAt(3));
            b2.setGrid(attack(b1.getGrid(), b2.getGrid(), xa1, ya1));

            if ( bas2 == 0) {
                System.out.println(p2name + "'s BattleShip sunk!\n");
                if ( carr2 == 0) {
                    System.out.println("Congratulation! " + p1name + " You have won" );
                    break BATTLE;
                }
            } else if ( carr2 == 0) {
                System.out.println(p2name + "'s Carrier sunk!\n");
                if ( bas2 == 0) {
                    System.out.println("Congratulation! " + p1name + " You have won\n");
                    b2.Printboard(b2.getGrid());
                    break BATTLE;
                }
            }

            b2.Printboard(b2.getGrid());
            System.out.println();
            System.out.println("-----------------------------------");
            System.out.println();

            //Second player turn
            b3.Printboard(b3.getGrid());
            System.out.println();
            System.out.print(p2name + " enter the coordinates for an attack: ");
            String attackposition2 = getInput6.nextLine();
            System.out.println();
            String getcoor6 = getXandY(attackposition2);
            int xa2 = Character.getNumericValue(getcoor6.charAt(1));
            int ya2 = Character.getNumericValue(getcoor6.charAt(3));
            b3.setGrid(attack1(b.getGrid(), b3.getGrid(), xa2, ya2));

            if ( bas1 == 0) {
                System.out.println(p1name + "'s BattleShip sunk!\n");
                if ( carr1 == 0) {
                    System.out.println("Congratulation! " + p2name + " You have won" );
                    break BATTLE;
                }
            } else if ( carr1 == 0) {
                System.out.println(p1name + "'s Carrier sunk!\n");
                if ( bas1 == 0) {
                    System.out.println("Congratulation! " + p2name + " You have won\n" );
                    b3.Printboard(b3.getGrid());
                    break BATTLE;
                }
            }
            b3.Printboard(b3.getGrid());
            System.out.println();
            System.out.println("-----------------------------------");
            System.out.println();
        }

    }

    public static String getXandY(String coordinates) {
        String result = "";
        for (int i = 0; i < coordinates.length(); i++) {
            if(coordinates.charAt(i) != ' ') {
                result += coordinates.charAt(i);
            }
        }
        return result;
    }

    public static String[][] attack(String[][] grid, String[][] grid1, int x, int y) {
        if (grid[x][y] == "c") {
            grid1[x][y] = "x";
            carr2--;
        } else if (grid[x][y] == "b") {
            grid1[x][y] = "x";
            bas2--;
        } else {
            System.out.println("You missed!\n");
            grid1[x][y] = "m";
        }
        return grid1;
    }

    public static String[][] attack1(String[][] grid, String[][] grid1, int x, int y) {
        if (grid[x][y] == "c") {
            grid1[x][y] = "x";
            carr1--;
        } else if (grid[x][y] == "b") {
            grid1[x][y] = "x";
            bas1--;
        } else {
            System.out.println("You missed!\n");
            grid1[x][y] = "m";
        }
        return grid1;
    }
}
