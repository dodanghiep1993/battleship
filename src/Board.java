public class Board {
    private String[][] grid= {{" ", "1", "2", "3", "4", "5", "6", "7", "8", "9"},
                              {"1", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"2", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"3", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"4", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"5", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"6", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"7", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"8", "~", "~", "~", "~", "~", "~", "~", "~", "~"},
                              {"9", "~", "~", "~", "~", "~", "~", "~", "~", "~"}};
    public static void Printboard(String[][] grid) {
        for (String[] symarray: grid) {
            for (String str: symarray) {
                System.out.print(str + "\t");
            }
            System.out.println();
        }
    }

//    public static String[][] attack(String[][] grid, String[][] grid1, String name, int x, int y, int z, int v) {
//        if (grid[x][y] == "c") {
//            grid1[x][y] = "x";
//            z--;
//        } else if (grid[x][y] == "b") {
//            grid1[x][y] = "x";
//            v--;
//        } else {
//            System.out.println("You missed!\n");
//            grid1[x][y] = "m";
//        }
//
//        if( z == 0) {
//            System.out.println(name + "'s Carrier sunk!");
//        } else if ( v == 0) {
//            System.out.println(name + "'s BattleShip sunk!");
//        } else if ( z == 0 && v == 0) {
//            System.out.println("Congratulation! " + name + " You have won" );
//            break BATTLE;
//        }
//        return grid1;
//    }

    public String[][] getGrid() {
        return grid;
    }

    public void setGrid(String[][] grid) {
        this.grid = grid;
    }
}
